//
//  ViewController.swift
//  SideMenu
//
//  Created by D_Developer on 12/12/17.
//  Copyright © 2017 D_Developer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var leftConstraintView: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var sideView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blurView.layer.cornerRadius = 15
        sideView.layer.shadowColor = UIColor.black.cgColor
        sideView.layer.shadowOpacity = 0.8
        sideView.layer.shadowOffset = CGSize(width: 5, height: 0 )
        
        leftConstraintView.constant = -175
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onGesture(_ sender: UIPanGestureRecognizer) {
        
        if sender.state == .began || sender.state == .changed{
            let translation = sender.translation(in: self.view).x
            if translation > 0 { // swipe right
                if leftConstraintView.constant < 20 {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.leftConstraintView.constant += translation / 7
                        self.view.layoutIfNeeded()
                    })
                }
            }else{ // swipe Left
                
                if leftConstraintView.constant > -175 {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.leftConstraintView.constant += translation / 5
                        self.view.layoutIfNeeded()
                    })
                }
                
            }
            
        }else if sender.state == .ended {
            if leftConstraintView.constant < -60 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.leftConstraintView.constant = -175
                    self.view.layoutIfNeeded()
                })
            }else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.leftConstraintView.constant = 0
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
}

